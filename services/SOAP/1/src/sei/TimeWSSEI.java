package sei;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface TimeWSSEI {
	
	@WebMethod
	public String getTimeAsString();

}
