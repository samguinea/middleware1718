package server;

import java.io.IOException;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.Name;
import javax.xml.soap.Node;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

public class ServerLogHandler implements SOAPHandler<SOAPMessageContext> {

	@Override
	public boolean handleMessage(SOAPMessageContext context) {
		
		Boolean outgoing = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		SOAPMessage msg = context.getMessage();
		
		if (!outgoing) {
			//deal with incoming message
			System.out.println("Dealing with incoming message...");
			try {
				SOAPBody body = msg.getSOAPBody();
				
				Node n = (Node) body.getChildNodes().item(0);
				int value = Integer.parseInt(n.getFirstChild().getFirstChild().getNodeValue());
				
				if (value==1) {
					System.out.println("Received arg0 -> 1");
					((SOAPElement)body.getFirstChild()).detachNode();
					SOAPBodyElement bel = body.addBodyElement(msg.getSOAPPart().getEnvelope().createName("addResponse", "ns2", "http://server/"));
					bel.addChildElement("return").addTextNode("1");
					return false;
				}
				
				
			} catch (SOAPException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else {
			//deal with outgoing message
			System.out.println("Dealing with outgoing message...");		
		}
		
		try {
			msg.writeTo(System.out);
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return true;
	}

	@Override
	public boolean handleFault(SOAPMessageContext context) {
		// TODO Auto-generated method stub
		System.out.println("Handle Fault in execution...");
		return true;
	}

	@Override
	public void close(MessageContext context) {
		// TODO Auto-generated method stub
		System.out.println("Close in execution...");
		
	}

	@Override
	public Set<QName> getHeaders() {
		// TODO Auto-generated method stub
		System.out.println("GetHeaders in execution...");
		return null;
	}

}
