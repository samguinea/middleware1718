package server;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface Calculator {
	@WebMethod
	public int add(int a, int b);
}
