package server;

import javax.jws.HandlerChain;
import javax.jws.WebService;


@WebService(endpointInterface="server.Calculator")
@HandlerChain(file="handler-chain-service.xml")
public class CalculatorImpl implements Calculator {

	@Override
	public int add(int a, int b) {
		// TODO Auto-generated method stub
		System.out.println("The backend has been executed...");
		return a+b;
	}

}
