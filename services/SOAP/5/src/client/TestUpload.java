package client;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.soap.SOAPBinding;

import support.ImageSOAPSEI;
import support.ImageSOAPSIBService;

public class TestUpload {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ImageSOAPSIBService service = new ImageSOAPSIBService();
		ImageSOAPSEI port = service.getImageSOAPSIBPort();
		
	/*	
		BindingProvider bProvider = (BindingProvider)port;
		SOAPBinding soapBinding = (SOAPBinding) bProvider.getBinding();
		soapBinding.setMTOMEnabled(false);
		System.out.println("Enabled -> " + soapBinding.isMTOMEnabled());
	*/	
	
		byte[] imageInByte = null;
		try {
			BufferedImage bImage = ImageIO.read(new File ("/Users/sam/Desktop/firefly.jpg"));
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(bImage,"jpg", baos);
			baos.flush();
			imageInByte = baos.toByteArray();
			baos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String result = port.uploadImage(imageInByte);
		System.out.println("Result -> " + result);
		
		
		

	}

}
