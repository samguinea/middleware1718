package client;

import java.util.List;

import support.ArrayOfString;
import support.Info;
import support.InfoSoapType;
import support.TFullTeamInfo;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Info service = new Info();
		InfoSoapType port = service.getInfoSoap();
		
		TFullTeamInfo teamInfo = port.fullTeamInfo("Italy");
		ArrayOfString forwards = teamInfo.getSForwards();
		List<String> forwardsList = forwards.getString();
		for (String s : forwardsList) {
			System.out.println(s);
		}

	}

}
