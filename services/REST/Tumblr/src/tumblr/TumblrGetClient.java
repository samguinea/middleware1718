package tumblr;

import java.awt.Image;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;


public class TumblrGetClient {

	/**
	 * 
	 * appName: MyTumblrAccess
	 * consumer key: 
	 * secret key: 
	 * @param args
	 */
	
	
	private static final String PROTECTED_RESOURCE_URL = "http://api.tumblr.com/v2/blog/samguinea.tumblr.com/posts/photo?";
			
			
			//api_key=}&[optional-params=]";
	
	private static final String CONSUMER_KEY = "";

	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Client client = ClientBuilder.newClient();
		
		String uri = PROTECTED_RESOURCE_URL + "api_key="+CONSUMER_KEY;
		 
		WebTarget target = client.target(uri);
		
		Invocation.Builder invocationBuilder = target.request(MediaType.APPLICATION_JSON);
 
		Response response = invocationBuilder.get();
		
		if (response.getStatus()!=200){
			throw new RuntimeException("Failed: HTTP Error Code :"+response.getStatus());			
		}
		
		System.out.println("Response status: " + response.getStatus());
		String jsonContent = response.readEntity(String.class);
		
		//System.out.println(jsonContent);
		
		
		JSONObject jobj = new JSONObject(jsonContent);
		
		System.out.println(jobj.getJSONObject("response"));	
		
		int hSize = (Integer) jobj.getJSONObject("response").getJSONArray("posts").getJSONObject(0).getJSONArray("photos").getJSONObject(0).getJSONObject("original_size").get("width");
		int vSize = (Integer) jobj.getJSONObject("response").getJSONArray("posts").getJSONObject(0).getJSONArray("photos").getJSONObject(0).getJSONObject("original_size").get("height");
		String imageURLString = (String) jobj.getJSONObject("response").getJSONArray("posts").getJSONObject(0).getJSONArray("photos").getJSONObject(0).getJSONObject("original_size").get("url");
		
		//Show the first image in the blog
		
		URL imageURL;
		Image image;
		try {
			imageURL = new URL(imageURLString);
			System.out.println("Downloading image...");
			image = ImageIO.read(imageURL);
			JFrame frame = new JFrame();
			frame.setSize(hSize, vSize);
			JLabel label = new JLabel(new ImageIcon(image));
			frame.add(label);
		    frame.setVisible(true);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		
	}

}
