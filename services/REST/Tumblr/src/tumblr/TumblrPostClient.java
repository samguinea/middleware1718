package tumblr;
import java.util.Scanner;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.TumblrApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;



public class TumblrPostClient {

	/**
	 * @param args
	 * 
	 * appName: MyTumblrAccess
	 * consumer key: 
	 * secret key: 
	 * 
	 * 
	 */
	
	private static final String PROTECTED_RESOURCE_URL = "http://api.tumblr.com/v2/blog/samguinea.tumblr.com/post";
	
	private static final String CONSUMER_KEY = "";
	private static final String SECRET_KEY = "";

	private static Scanner in;
	
	public static void main(String[] args) {
		
		OAuthService service = new ServiceBuilder().provider(TumblrApi.class).apiKey(CONSUMER_KEY).apiSecret(SECRET_KEY).callback("http://localhost:8080").build();
		in = new Scanner(System.in);
		
		System.out.println("=== Tumblr's OAuth Workflow ===");
	    System.out.println();

	    // Obtain the Request Token
	    System.out.println("Fetching the Temporary Request Token...");
	    Token requestToken = service.getRequestToken();
	    System.out.println("Got the Request Token!");
	    System.out.println();

	    System.out.println("Now go and authorize MyTumblrAccess here:");
	    System.out.println(service.getAuthorizationUrl(requestToken));
	    System.out.println("And paste the verifier here (remember to exclude the #)");
	    System.out.print(">>");
	    Verifier verifier = new Verifier(in.nextLine());
	    System.out.println();

	    // Trade the Request Token and Verfier for the Access Token
	    System.out.println("Trading the Request Token for an Access Token...");
	    Token accessToken = service.getAccessToken(requestToken, verifier);
	    System.out.println("Got the Access Token!");
	    System.out.println("(if your curious it looks like this: " + accessToken + " )");
	    System.out.println();

	    // Now let's go and create a protected resource!
	    System.out.println("Now we're going to create a protected resource...");
	    OAuthRequest request = new OAuthRequest(Verb.POST, PROTECTED_RESOURCE_URL);
    
	    //Send a photo through source
	    
	    request.addBodyParameter("type", "photo");
	    request.addBodyParameter("caption", "The Seven Sisters!");
	    request.addBodyParameter("source", "http://home.deib.polimi.it/guinea/sfSisters.jpg");
	    
	    service.signRequest(accessToken, request);
	    
	    Response response = request.send();
	    System.out.println("Got it! Lets see what we found...");
	    System.out.println();
	    System.out.println(response.getBody());

		
		
	}

}
