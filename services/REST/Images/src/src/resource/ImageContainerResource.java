package src.resource;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

@Path("/imageContainer")
public class ImageContainerResource {
	
	@Context UriInfo uriInfo;

	@Path("{imageName}")
	public ImageResource getImageResource(@PathParam(value="imageName") String imageName) {
		return new ImageResource(imageName);
		
	}
	
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	 public Response postForm(
	         @FormDataParam("file") InputStream file,
	         @FormDataParam("file") FormDataContentDisposition fileDisposition) {
		
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		int nRead;
		byte[] data = new byte[16384];
		try {
			while ((nRead = file.read(data, 0, data.length)) != -1) {
			  buffer.write(data, 0, nRead);
			}
			buffer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ImageStorage.IS.addImage(fileDisposition.getFileName(), buffer.toByteArray());
		String uriString = uriInfo.getAbsolutePath().toString() + fileDisposition.getFileName();
		URI uri = URI.create(uriString);
		return Response.created(uri).build();
	}
	
	@GET
	@Produces ("application/json")
	public ImageContainer getContainer() {
		return ImageStorage.IS.getImageContainer();
		
	}
	

}
