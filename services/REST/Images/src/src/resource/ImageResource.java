package src.resource;


import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Produces("image/jpeg")
public class ImageResource {

	String imageName;
	
	public ImageResource(String imageName) {
		this.imageName = imageName;
	}
	
	@GET
	public Response getImage() {	
		System.out.println("Received request with imageName: " + imageName);
		byte[] b = ImageStorage.IS.getImage(imageName);
		return Response.ok(b).build();
		
	}
	
	@DELETE
	public Response deleteImage() {
		ImageStorage.IS.removeImage(imageName);
		return Response.ok().build();
	}

}
