package src.resource;


import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ImageContainer {
	
	List<Image> image;

	public List<Image> getContainerNames() {
		return image;
	}

	public void setContainerNames(List<Image> containerNames) {
		this.image = containerNames;
	}

	public ImageContainer() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ImageContainer(List<Image> containerNames) {
		this.image = containerNames;
	}

	public ImageContainer(Image[] imageArray) {
		// TODO Auto-generated constructor stub
		this.image = new ArrayList<Image>();
		for (Image i : imageArray) {
			this.image.add(i);
		}
	}
	
	

}
