package src.json.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="LibroType", propOrder={"titolo", "autore", "prezzo"})
public class Libro {
	
	@XmlElement(name="Titolo", required=true)
	protected String titolo;
	
	@XmlElement(name="Autore", required=true)
	protected String autore;
	
	@XmlElement(name="Prezzo", required=true)
	protected int prezzo;
	
	public String getTitolo() {
		return titolo;
	}
	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}
	public String getAutore() {
		return autore;
	}
	public void setAutore(String autore) {
		this.autore = autore;
	}
	public int getPrezzo() {
		return prezzo;
	}
	public void setPrezzo(int prezzo) {
		this.prezzo = prezzo;
	}

	
	

}

