package src.json.rest;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="Libreria")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="LibreriaType")
public class Libreria {
	
	@XmlElement(name="Libro", required=false)
	protected List<Libro> libri = new ArrayList<Libro>();
	
	@XmlAttribute(name="NomeLibreria", required=true)
	protected String nome;

	public List<Libro> getLibri() {
		return libri;
	}

	public void setLibri(List<Libro> libri) {
		this.libri = libri;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	
	
	

}
