package src.json.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;



@Path("/libri")
public class JSONResource {
	
	@GET
	@Produces ("application/xml")
	@Path("/xml")
	public Libreria getXML() {
		Libreria l = new Libreria();
		
		Libro libro1 = new Libro();
		libro1.setAutore("J.R.R. Tolkien");
		libro1.setPrezzo(50);
		libro1.setTitolo("Il Signore degli Anelli");
		
		Libro libro2 = new Libro();
		libro2.setAutore("J.R.R. Tolkien");
		libro2.setPrezzo(20);
		libro2.setTitolo("Lo Hobbit");
		
		Libro libro3 = new Libro();
		libro3.setAutore("J.R.R. Tolkien");
		libro3.setPrezzo(30);
		libro3.setTitolo("Silmarillion");
		
		l.getLibri().add(libro1);
		l.getLibri().add(libro2);
		l.getLibri().add(libro3);
		
		return l;
	}
	
	@GET
	@Produces ("application/json")
	@Path("/json")
	public Libreria getJSON() {
		Libreria l = new Libreria();
		
		Libro libro1 = new Libro();
		libro1.setAutore("J.R.R. Tolkien");
		libro1.setPrezzo(50);
		libro1.setTitolo("Il Signore degli Anelli");
		
		Libro libro2 = new Libro();
		libro2.setAutore("J.R.R. Tolkien");
		libro2.setPrezzo(20);
		libro2.setTitolo("Lo Hobbit");
		
		Libro libro3 = new Libro();
		libro3.setAutore("J.R.R. Tolkien");
		libro3.setPrezzo(30);
		libro3.setTitolo("Silmarillion");
		
		l.getLibri().add(libro1);
		l.getLibri().add(libro2);
		l.getLibri().add(libro3);
		
		return l;
	}
	


}







