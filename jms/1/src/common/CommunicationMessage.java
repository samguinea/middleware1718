package common;

import java.io.Serializable;

public class CommunicationMessage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3612434157713565391L;

	private String name;
	private String message;
	
	public CommunicationMessage(String username, String message) {
		super();
		this.name = username;
		this.message = message;
	}
	
	public String getUsername() {
		return name;
	}
	public void setUsername(String username) {
		this.name = username;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
	
	
}
