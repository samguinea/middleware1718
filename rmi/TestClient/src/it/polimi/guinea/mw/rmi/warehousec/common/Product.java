package it.polimi.guinea.mw.rmi.warehousec.common;

import java.io.Serializable;

public class Product implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7016498236763698532L;
	
	private String uniqueID;
	private double price;
	
	public Product(String uniqueID, double price) {
		// TODO Auto-generated constructor stub
		this.uniqueID = uniqueID;
		this.price = price;
	}
	
	public String getUniqueID() {
		return uniqueID;
	}
	public void setUniqueID(String uniqueID) {
		this.uniqueID = uniqueID;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getDescription() {
		return "*** Description provided by Product class**** " + this.uniqueID + " -> " + this.price;
	}
	
	
	
}
