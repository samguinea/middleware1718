package it.polimi.guinea.mw.rmi.warehouse.common;

import java.io.Serializable;

public class ActiveProduct implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String description;
	private double price;

	
	public ActiveProduct(String description, double price) {
		super();
		this.description = description;
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public double getPrice() {
		return price;
	}
	
	
}
