package it.polimi.guinea.mw.rmi.warehouse.client;

import it.polimi.guinea.mw.rmi.warehouse.common.ActiveProduct;
import it.polimi.guinea.mw.rmi.warehouse.common.ActiveWarehouse;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.List;

public class ActiveWarehouseClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Registry registry = null;
		try {
			registry = LocateRegistry.getRegistry();
			System.out.println("Registry bindings...");
			String[] bindings = registry.list();
			for (int i=0; i<bindings.length; i++) {
				System.out.println(bindings[i]);
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String remoteObjectName = "warehouse";
		ActiveWarehouse wh = null;
		try {
			wh = (ActiveWarehouse) registry.lookup(remoteObjectName);
		} catch (RemoteException | NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//System.out.println("Reached this point...");
		
		try {
			System.out.println("Location -> " + wh.getLocation());
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			wh.add("toaster", new ActiveProduct("cool toaster", 100));
			wh.add("microwave",  new ActiveProduct("cool microwave",  200));
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			
			System.out.println("Price of C -> " + wh.getPrice("C"));
			System.out.println("Price of toaster -> " + wh.getPrice("cool toaster"));
			
			List<String> keywords = new ArrayList<String>();
			keywords.add("microwave");
			ActiveProduct ap = wh.getProduct(keywords);
			System.out.println("Found product -> " + ap.getDescription() + " - " + ap.getPrice());
			
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		
		
		
		
	}

}
