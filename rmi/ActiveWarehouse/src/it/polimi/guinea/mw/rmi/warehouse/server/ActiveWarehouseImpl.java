package it.polimi.guinea.mw.rmi.warehouse.server;

import it.polimi.guinea.mw.rmi.warehouse.common.ActiveProduct;
import it.polimi.guinea.mw.rmi.warehouse.common.ActiveWarehouse;

import java.io.IOException;
import java.rmi.MarshalledObject;
import java.rmi.RemoteException;
import java.rmi.activation.Activatable;
import java.rmi.activation.ActivationID;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActiveWarehouseImpl extends Activatable implements ActiveWarehouse {

	private Map<String, ActiveProduct> products;
	private String loc;
	
	public ActiveWarehouseImpl(ActivationID id, MarshalledObject<ActiveWarehouseConfig> data) throws RemoteException {
		super(id, 0);
		//add extra code for initialization from incoming data
		products = new HashMap<String, ActiveProduct>();
		List<ActiveProduct> prods = null;
		try {
			prods = data.get().getList();
			for (ActiveProduct ap : prods) {
				products.put(ap.getDescription(), ap);
			}
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.loc = "Server number 1";
	}
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2451453064192286115L;

	@Override
	public double getPrice(String description) throws RemoteException {
		for (ActiveProduct p : this.products.values()) {
			if (p.getDescription().equals(description)) {
				return p.getPrice();
			}
		}
		return -1;
	}

	@Override
	public ActiveProduct getProduct(List<String> keywords) throws RemoteException {
		for (String keyword : keywords) {
			ActiveProduct p = this.products.get(keyword);
			if (p!=null) return p;
		}
		return null;
	}

	@Override
	public String getLocation() throws RemoteException {
		return this.loc;
	}

	@Override
	public void add(String keyword, ActiveProduct p) throws RemoteException {
		if (!products.containsKey(keyword)) {
			products.put(keyword, p);
		}
	}

}
