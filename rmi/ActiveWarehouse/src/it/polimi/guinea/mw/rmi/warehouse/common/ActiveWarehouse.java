package it.polimi.guinea.mw.rmi.warehouse.common;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface ActiveWarehouse extends Remote {

	double getPrice(String description) throws RemoteException;
	ActiveProduct getProduct(List<String> keywords) throws RemoteException;
	String getLocation() throws RemoteException;
	void add(String keyword, ActiveProduct p) throws RemoteException;
	
	
}
