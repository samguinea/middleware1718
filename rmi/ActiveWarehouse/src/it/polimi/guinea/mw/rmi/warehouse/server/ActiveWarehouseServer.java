package it.polimi.guinea.mw.rmi.warehouse.server;

import it.polimi.guinea.mw.rmi.warehouse.common.ActiveWarehouse;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.MarshalledObject;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.activation.Activatable;
import java.rmi.activation.ActivationDesc;
import java.rmi.activation.ActivationException;
import java.rmi.activation.ActivationGroup;
import java.rmi.activation.ActivationGroupDesc;
import java.rmi.activation.ActivationGroupID;
import java.util.Properties;

public class ActiveWarehouseServer {

	public static void main(String[] args) {
		
		Properties props = new Properties();
		props.put("java.security.policy", "myrmi.policy");
		
		ActivationGroupDesc groupDescription = new ActivationGroupDesc(props, null);
		try {
			ActivationGroupID gid = ActivationGroup.getSystem().registerGroup(groupDescription);
			
			String location = "http://localhost:8888/ActiveWarehouse/";
			MarshalledObject<ActiveWarehouseConfig> data = new MarshalledObject<ActiveWarehouseConfig>(new ActiveWarehouseConfig());
			ActivationDesc activationDescription = new ActivationDesc(gid, "it.polimi.guinea.mw.rmi.warehouse.server.ActiveWarehouseImpl", location, data);
			
			ActiveWarehouse activeWarehouse = (ActiveWarehouse)Activatable.register(activationDescription);
			System.out.println("Stub created for the activatable object...");
			
			Naming.rebind("warehouse", activeWarehouse);
			
		} catch (RemoteException | ActivationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
		

	}

}
