package it.polimi.guinea.mw.rmi.warehouse.server;

import it.polimi.guinea.mw.rmi.warehouse.common.ActiveProduct;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ActiveWarehouseConfig implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3928534361778834732L;
	static List<ActiveProduct> prods;
	
	static {
		prods = new ArrayList<ActiveProduct>();
		prods.add(new ActiveProduct("A", 1.0));
		prods.add(new ActiveProduct("B", 2.0));
		prods.add(new ActiveProduct("C", 3.0));
	}
	
	public List<ActiveProduct> getList() {
		return prods;
	}
	
	

}
