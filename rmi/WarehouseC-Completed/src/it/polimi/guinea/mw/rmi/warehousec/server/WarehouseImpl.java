package it.polimi.guinea.mw.rmi.warehousec.server;

import it.polimi.guinea.mw.rmi.warehousec.common.Product;
import it.polimi.guinea.mw.rmi.warehousec.common.Warehouse;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;

public class WarehouseImpl extends UnicastRemoteObject implements Warehouse {

	private Map<String, Double> products;
	
	protected WarehouseImpl() throws RemoteException {
		super();
		products = new HashMap<String, Double>();
		products.put("A", 1.0);
		products.put("B", 2.0);
		products.put("C", 3.0);
		products.put("book", 4.0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -1661130455538223850L;

	@Override
	public double getPrice(String uniqueID) throws RemoteException {
		if (products.containsKey(uniqueID)) {
			return products.get(uniqueID);
		}
		return -1;
	}

	@Override
	public void add(Product newProduct) throws RemoteException {
		if (!products.containsKey(newProduct.getUniqueID())) {
			products.put(newProduct.getUniqueID(), newProduct.getPrice());
		}	
	}

	@Override
	public Product getProduct(String uniqueID) throws RemoteException {		
		if (products.get(uniqueID)!= null) {
			if (uniqueID.equals("book")) {
				Book returnBook = new Book(uniqueID, products.get(uniqueID),"isbn_1234");
				return returnBook;
			}
			Product returnProduct = new Product(uniqueID, products.get(uniqueID));
			return returnProduct;
		}
		return null;
	}

}
