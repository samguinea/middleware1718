package it.polimi.guinea.mw.rmi.warehousec.server;

import it.polimi.guinea.mw.rmi.warehousec.common.Product;

public class Book extends Product {

	/**
	 * 
	 */
	private static final long serialVersionUID = -661522211170450941L;

	private String isbn;

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public Book(String uniqueID, double price, String isbn) {
		super(uniqueID, price);
		this.isbn = isbn;
	}
	
	public String getDescription() {
		return "*** Description provided by Book class*** " + this.getUniqueID() + "("+this.getIsbn()+") - "+ this.getPrice(); 
	}

	
	
	
	
	
}
