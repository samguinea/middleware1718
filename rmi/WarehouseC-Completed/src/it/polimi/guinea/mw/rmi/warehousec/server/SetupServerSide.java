package it.polimi.guinea.mw.rmi.warehousec.server;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import it.polimi.guinea.mw.rmi.warehousec.common.Warehouse;

public class SetupServerSide {

	public static void main(String[] args) {
		
		//Instantiate the Remote Object
		Warehouse wh = null;
		try {
			wh = new WarehouseImpl();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.setProperty("java.rmi.server.codebase", "http://localhost:8888/Warehouse/");
		System.out.println("Codebase -> " + System.getProperty("java.rmi.server.codebase"));
		
		
		//Register the Remote Object
		try {
			Registry registry = LocateRegistry.getRegistry();
			registry.bind("warehouse", wh);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AlreadyBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Waiting for client interaction...");
	

	}

}
